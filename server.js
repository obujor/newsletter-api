const express = require('express');
const https = require('https');
const fs = require('fs');
const path = require('path');

require('dotenv').config();

const newsletterRouter = require('./src/router');

const app = express();

app.use(express.json());

app.get('/hello', (req, res) => res.json({
    messasge: 'hi there tickete devs'
}));

app.use('/newsletter', newsletterRouter);

const options = {
    key: fs.readFileSync(path.join(__dirname, process.env.SSL_KEY_FILE)),
    cert: fs.readFileSync(path.join(__dirname, process.env.SSL_CERTIFICATE_FILE)),
    passphrase: process.env.SSL_PASSPHRASE
};
const port = process.env.API_PORT;
https.createServer(options, app)
    .listen(port, () =>
        console.log('Listening on port %d', port)
    );