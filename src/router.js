const express = require('express');

const Subscribe = require('./Subscribe');
const Users = require('./Users');
const utils = require('./utils');
const storage = require('./storage');

const router = express.Router();

// Token check middleware
router.use((req, res, next) => {
    const { token } = Object.assign({}, req.body, req.query);
    const sendNotValid = () => utils.sendError(res, 401, 'Token not valid');

    if (!token || token.length != 64)
        return sendNotValid();

    storage.existToken(token)
        .then(exists => exists ? next() : sendNotValid())
        .catch(sendNotValid)
});

router.use('/subscribe', Subscribe.router);
router.use('/users', Users.router);

module.exports = router;