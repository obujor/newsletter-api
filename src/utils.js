const sendError = (res, code, err) => {
    res.status(code);
    res.json({error: err});
}

module.exports = {
    sendError,
}