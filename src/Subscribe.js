const express = require('express');
const validator = require('newsletter-field-validation');

const utils = require('./utils');
const storage = require('./storage');

const router = express.Router();

router.post('*', (req, res) => {
    if (!req.body || Object.keys(req.body).length === 0) {
        return utils.sendError(res, 400, 'No input provided');
    }
    // Keep only the fields we need
    const { name,
            surname,
            birthDate,
            email,
            zipcode
        } = req.body;
    const fields = { name,
                    surname,
                    birthDate,
                    email,
                    zipcode };

    const invalidFields = validFields(fields);
    if (invalidFields.length > 0) {
        return utils.sendError(res, 400, `Invalid fields: ${invalidFields.join()}`);
    }
    
    storage.saveUser(fields)
        .then(() => {
            res.json({success: true});
        })
        .catch((err) => {
            return utils.sendError(res, 500, `Saving error: ${err}`);
        })
});

const validFields = ({
        name,
        surname,
        birthDate,
        email,
        zipcode
    }) => {
    const invalidFields = [];
    if (!validator.isValidName(name))
        invalidFields.push('name');
    if (!validator.isValidSurname(surname))
        invalidFields.push('surname');
    if (!validator.isValidBirthDate(birthDate))
        invalidFields.push('birthDate');
    if (!validator.isValidEmail(email))
        invalidFields.push('email');
    if (!validator.isValidZipCode(zipcode))
        invalidFields.push('zipcode');

    return invalidFields;
}

exports.router = router;