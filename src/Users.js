const express = require('express');
const validator = require('newsletter-field-validation');

const utils = require('./utils');
const storage = require('./storage');

const router = express.Router();

router.get('/list', (req, res) => {
    storage.getUsers()
        .then((users) => {
            res.json({success: true, users});
        })
        .catch((err) => {
            return utils.sendError(res, 500, `Getting error: ${err}`);
        })
});

router.get('/getByEmail', (req, res) => {
    const { email } = req.query;
    if (!email) {
        return utils.sendError(res, 400, 'No input provided');
    }
    if (!validator.isValidEmail(email)) {
        return utils.sendError(res, 400, `Not valid provided email`);
    }
    storage.getUsers({ email })
        .then((users) => {
            if (users.length > 0) {
                res.json({success: true, user: users[0]});
            } else {
                utils.sendError(res, 404, `User not found`);
            }
        })
        .catch((err) => {
            return utils.sendError(res, 500, `Getting error: ${err}`);
        })
});

exports.router = router;