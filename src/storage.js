const Datastore = require('nedb');
const path = require('path');

require('dotenv').config();

const dbTokens = new Datastore({
    filename: path.join(__dirname, '../', process.env.TOKENS_DB_FILE),
    autoload: true
});

const dbUsers = new Datastore({
    filename: path.join(__dirname, '../', process.env.USERS_DB_FILE),
    autoload: true
});

const saveUser = userData =>
    saveItem(dbUsers, userData)

const getUsers = (filters = {}) =>
    getItems(dbUsers, filters)
        .then(users => users.map(({_id, ...data}) => data))

const saveToken = token =>
    saveItem(dbTokens, { token })

const existToken = token =>
    getItems(dbTokens, { token })
        .then(tokens => !!tokens.length)

const saveItem = (db, item) =>
    new Promise((resolve, reject) => {
        db.insert(item, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });

const getItems = (db, filters = {}) =>
    new Promise((resolve, reject) => {
        db.find(filters, (err, items) => {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }
        });
    });

module.exports = {
    saveUser,
    getUsers,
    saveToken,
    existToken
}