const crypto = require('crypto');

const storage = require('./storage');

const token = crypto.randomBytes(32).toString('hex');

storage.saveToken(token)
    .then(() => console.log(`Token created: ${token}`))
    .catch(err => console.error(err))