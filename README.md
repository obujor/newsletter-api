# newsletter-api

Simple newsletter REST API implementation for Tickete application

## Configuration
Before using this service you have to configure it by changing `.env` file.

## Usage server
In order to start server you have to run `npm start`

In order to generate a new API TOKEN you have to run `npm run generate-token`

## Usage client API
Every endpoint requires an API TOKEN, you have to pass it by using the `token` parameter, see examples below.

### Endpoints
#### POST /newsletter/subscribe
The body playload have to be a json with `application/json` content-type.

Here are the required JSON fields:
* name
* surname
* birthDate
* email
* zipcode

Example:
```javascript
{
  "name": "Pinco",
  "surname": "Rossi",
  "birthDate": "10/10/1960",
  "email": "c.rossi@gmail.com",
  "zipcode": "40010"
}
```

cUrl request example:
```bash
curl -X POST \
  'https://localhost:3000/newsletter/subscribe?token=API_TOKEN' \
  -H 'Content-Type: application/json' \
  -d '{
	"name": "Pinco",
	"surname": "Rossi",
	"birthDate": "10/10/1960",
	"email": "c.rossi@gmail.com",
	"zipcode": "40010"
}'
```

The result is a HTTP error or success:
```javascript
{
  "success": true
}
```

#### GET /users/list

No inputs required except token.

cUrl request example:
```bash
curl -X GET \
  'https://localhost:3000/newsletter/users/list?token=API_TOKEN'
```

Result example:
```javascript
{
    "success": true,
    "users": [
        {
            "name": "Pinco",
            "surname": "Rossi",
            "birthDate": "10/10/1960",
            "email": "c.rossi@gmail.com",
            "zipcode": "40010"
        },
        {
            "name": "Mario",
            "surname": "Rossi",
            "birthDate": "10/10/1990",
            "email": "m.rossi@gmail.com",
            "zipcode": "40010"
        }
    ]
}
```

#### GET /users/getByEmail

The only one input required except token is `email`.

cUrl request example:
```bash
curl -X GET \
  'https://localhost:3000/newsletter/users/getByEmail?email=l.rossi@gmail.com&token=API_TOKEN' 
```

Result example:
```javascript
{
    "success": true,
    "user": {
        "name": "Luca",
        "surname": "Rossi",
        "birthDate": "10/10/1960",
        "email": "l.rossi@gmail.com",
        "zipcode": "40010"
    }
}
```